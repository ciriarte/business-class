const main = () => {
    const $ = document;

    const build_pattern = /\/teams\/(\w+)\/pipelines\/(.+?)\/jobs\/(.+?)\/builds\/(\d+)/;
    const normalize_uri_pattern = /(\w+\.?\w+)(?:\/|\:)((?:\w+|\-)+)\/((?:\w+|\-)+)(?:\.git)?$/

    const matches = build_pattern.exec($.location.pathname);

    if (!matches) {
        return
    }

    const team = matches[1];
    const name = matches[2];
    const jobName = matches[3];
    const buildNumber = matches[4];

    const scm = fetch(`/api/v1/teams/${team}/pipelines/${name}/config`)
        .then(res => res.json())
        .then(pipeline => {
            let resources = pipeline.config.resources;
            resources = resources.filter(item => {
                if (item.type === 'git') {
                    return item;
                }
            });
            return resources.map(item => {
                const matches = normalize_uri_pattern.exec(item.source.uri);
                const domain = matches[1];
                const org = matches[2];
                const repo = matches[3];

                return {
                    'name': item.name,
                    org,
                    repo,
                    domain
                }
            });
        });

    const inputs = fetch(`/api/v1/teams/${team}/pipelines/${name}/jobs/${jobName}`)
        .then(res => res.json())
        .then(job => job.inputs);

    const pipelineRendered = new Promise((resolve, reject) => {
        const checkIfLoaded = () => {
            const newResources = document.getElementsByClassName('first-occurrence');
            if (newResources.length === 0) {
                setTimeout(checkIfLoaded, 1000)
                return
            }

            const res = {};
            for (r of newResources) {
                res[r.dataset.stepName] = r;
            }

            resolve(res);
        };
    
        checkIfLoaded();
    });

    Promise.all([scm, inputs, pipelineRendered])
        .then(values => {
            const [repos, inputs, newResources] = values;

            for (let key of Object.keys(newResources)) {
                const input = inputs.find(i => i.name === key);
                const repo = repos.find(i => i.name === input.resource);

                if (!repo) {
                    continue;
                }

                const res = newResources[key];
                const node = res.getElementsByClassName('header')[0]
                                .getElementsByClassName('dict-value')[0];

                node.innerHTML = `<a class='scm' href='https://${repo.domain}/${repo.org}/${repo.repo}/commit/${node.textContent}' target='_blank'>${node.textContent}</a>`;
            }
        })
        .catch(error => {
            console.log(`Config ${error}`);
        });
}

window.addEventListener('message', () => setTimeout(main, 300));

main();